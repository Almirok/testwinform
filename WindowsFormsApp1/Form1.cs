﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        private Form1 _form;

        int sum = 0;
        string answer, cap = "Эспрессо + Молоко + Вода", latte = "Эспрессо + Молоко";
        public  List<RadioButton> Radios()
        {
            List<RadioButton> rb = new List<RadioButton>()
            {
                radioWater,
                radioEspresso,
                radioLatte,
                radioKapuchino,
                radioBlackTea,
                radioGreenTea,
                radioBread,
                radioBun,
                radioChips,
                radioCookies
            };
            return rb;
        }
        public Form1()
        {
            InitializeComponent();
        }

        private void CountRadio(Dictionary<RadioButton, int> radioButtons)
        {
            foreach (var x in radioButtons)
            {
                if (x.Key.Checked)
                {
                    sum += x.Value;
                    if (x.Key.Text != "Хлеб")
                    {
                        if (x.Key.Text != "Булочка")
                        {
                            
                            if (x.Key.Text == "Латте")
                            {
                                answer += x.Key.Text + " (" + latte + ") \n";
                            }
                            else if (x.Key.Text == "Капучино")
                            {
                                answer += x.Key.Text + " (" + cap + ") \n";
                            }
                            else
                            {
                                answer += x.Key.Text + "\n";
                            }
                        }
                       
                    }
                    
                    if (x.Key.Text == "Хлеб" || x.Key.Text == "Булочка")
                    {
                        if (checkChees.Checked && checkHam.Checked)
                        {
                            answer += "Бутерброд" + " ( " + x.Key.Text + " + Ветчина + Сыр) \n";
                        }
                        else
                        {
                            if (checkChees.Checked)
                            {
                                answer += "Бутерброд" + " ( " + x.Key.Text + " + Сыр) \n";
                            }

                            if (checkHam.Checked)
                            {
                                answer += "Бутерброд" + " ( " + x.Key.Text + " + Ветчина) \n";
                            }
                            if (checkJam.Checked)
                            {
                                answer += "Бутерброд" + " ( " + x.Key.Text + " + Джем) \n";
                            }
                        }
                        
                    }
                    
                }
            }
        }//проверка радиокнопок

        private void CountChexbox(Dictionary<CheckBox, int> chexBoxes)
        {
            foreach (var x in chexBoxes)
            {
                if (x.Key.Checked)
                {
                    sum += x.Value;
                    if (radioChips.Checked)
                    {
                        answer += x.Key.Text + " \n";
                    }
                    else if (radioCookies.Checked)
                    {
                        answer += x.Key.Text + " \n";
                    }

                }
            }
        }//проверка чекбоксов

        private void issueButton_Click(object sender, EventArgs e)
        {
            Dictionary<RadioButton, int> Waters = new Dictionary<RadioButton, int>
            {
                {radioWater, 20 },
                {radioEspresso, 50 },
                {radioLatte, 60 },
                {radioKapuchino, 70 },
                {radioBlackTea, 25 },
                {radioGreenTea, 25 }
            };
            Dictionary<RadioButton, int> Foods = new Dictionary<RadioButton, int>
            {
                {radioBread, 10 },
                {radioBun, 15 },
                {radioChips, 34 },
                {radioCookies, 29 },
            };
            Dictionary<CheckBox, int> addWaters = new Dictionary<CheckBox, int>
            {
                {checkMilk, 10 },
                {checkSyrup, 5 },
                {checkSugar, 3 * (int)countSugar.Value },
            };
            Dictionary<CheckBox, int> addFoods = new Dictionary<CheckBox, int>
            {
                {checkHam, 15 },
                {checkChees, 10 },
                {checkJam, 7 },
            };

            CountRadio(Waters);
            CountRadio(Foods);
            CountChexbox(addWaters);
            CountChexbox(addFoods);

            textPrice.Clear();

            if (checkComplex.Checked)
            {
                textPrice.SelectedText = "90";
            }
            else
            {
                textPrice.SelectedText = sum.ToString();
            }

            MessageBox.Show(answer);

            answer = "";
            sum = 0;

        }//нажатие на кнопку "Выдать"

        private void radioWater_CheckedChanged(object sender, EventArgs e)
        {
            checkMilk.CheckState = CheckState.Unchecked;
        }//ограничение вода + молоко

        private void checkMilk_CheckedChanged(object sender, EventArgs e)
        {
            if (radioWater.Checked)
            {
                checkMilk.CheckState = CheckState.Unchecked;
            }
        }//ограничение вода + молоко

        private void checkComplex_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkHam_CheckedChanged(object sender, EventArgs e)
        {
            if (radioChips.Checked || radioCookies.Checked)
            {
                checkHam.CheckState = CheckState.Unchecked;
                checkChees.CheckState = CheckState.Unchecked;
            }
        }//ограничение чипсы/печенье + Ветчина/Сыр

        private void checkChees_CheckedChanged(object sender, EventArgs e)
        {
            if (radioChips.Checked || radioCookies.Checked)
            {
                checkHam.CheckState = CheckState.Unchecked;
                checkChees.CheckState = CheckState.Unchecked;
            }
        }//ограничение чипсы/печенье + Ветчина/Сыр
    }
}
