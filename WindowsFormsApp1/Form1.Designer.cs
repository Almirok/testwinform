﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>


        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.waterBox = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.radioGreenTea = new System.Windows.Forms.RadioButton();
            this.radioBlackTea = new System.Windows.Forms.RadioButton();
            this.radioKapuchino = new System.Windows.Forms.RadioButton();
            this.radioLatte = new System.Windows.Forms.RadioButton();
            this.radioEspresso = new System.Windows.Forms.RadioButton();
            this.radioWater = new System.Windows.Forms.RadioButton();
            this.foodBox = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.radioBread = new System.Windows.Forms.RadioButton();
            this.label8 = new System.Windows.Forms.Label();
            this.radioBun = new System.Windows.Forms.RadioButton();
            this.label9 = new System.Windows.Forms.Label();
            this.radioChips = new System.Windows.Forms.RadioButton();
            this.label10 = new System.Windows.Forms.Label();
            this.radioCookies = new System.Windows.Forms.RadioButton();
            this.addWaterBox = new System.Windows.Forms.GroupBox();
            this.countSugar = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.checkSugar = new System.Windows.Forms.CheckBox();
            this.checkSyrup = new System.Windows.Forms.CheckBox();
            this.checkMilk = new System.Windows.Forms.CheckBox();
            this.addFoodBox = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.checkJam = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.checkChees = new System.Windows.Forms.CheckBox();
            this.label13 = new System.Windows.Forms.Label();
            this.checkHam = new System.Windows.Forms.CheckBox();
            this.checkComplex = new System.Windows.Forms.CheckBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.textPrice = new System.Windows.Forms.TextBox();
            this.issueButton = new System.Windows.Forms.Button();
            this.waterBox.SuspendLayout();
            this.foodBox.SuspendLayout();
            this.addWaterBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.countSugar)).BeginInit();
            this.addFoodBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // waterBox
            // 
            this.waterBox.Controls.Add(this.label6);
            this.waterBox.Controls.Add(this.label5);
            this.waterBox.Controls.Add(this.label4);
            this.waterBox.Controls.Add(this.label3);
            this.waterBox.Controls.Add(this.label2);
            this.waterBox.Controls.Add(this.label1);
            this.waterBox.Controls.Add(this.radioGreenTea);
            this.waterBox.Controls.Add(this.radioBlackTea);
            this.waterBox.Controls.Add(this.radioKapuchino);
            this.waterBox.Controls.Add(this.radioLatte);
            this.waterBox.Controls.Add(this.radioEspresso);
            this.waterBox.Controls.Add(this.radioWater);
            this.waterBox.Location = new System.Drawing.Point(24, 23);
            this.waterBox.Name = "waterBox";
            this.waterBox.Size = new System.Drawing.Size(183, 207);
            this.waterBox.TabIndex = 0;
            this.waterBox.TabStop = false;
            this.waterBox.Text = "Напитки";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(135, 178);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "25 руб.";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(135, 149);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "25 руб.";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(135, 117);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "70 руб.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(135, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "60 руб.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(135, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "50 руб.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(135, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "20 руб.";
            // 
            // radioGreenTea
            // 
            this.radioGreenTea.AutoSize = true;
            this.radioGreenTea.Location = new System.Drawing.Point(6, 178);
            this.radioGreenTea.Name = "radioGreenTea";
            this.radioGreenTea.Size = new System.Drawing.Size(92, 17);
            this.radioGreenTea.TabIndex = 5;
            this.radioGreenTea.TabStop = true;
            this.radioGreenTea.Text = "Чай зеленый";
            this.radioGreenTea.UseVisualStyleBackColor = true;
            // 
            // radioBlackTea
            // 
            this.radioBlackTea.AutoSize = true;
            this.radioBlackTea.Location = new System.Drawing.Point(6, 145);
            this.radioBlackTea.Name = "radioBlackTea";
            this.radioBlackTea.Size = new System.Drawing.Size(85, 17);
            this.radioBlackTea.TabIndex = 4;
            this.radioBlackTea.TabStop = true;
            this.radioBlackTea.Text = "Чай черный";
            this.radioBlackTea.UseVisualStyleBackColor = true;
            // 
            // radioKapuchino
            // 
            this.radioKapuchino.AutoSize = true;
            this.radioKapuchino.Location = new System.Drawing.Point(6, 113);
            this.radioKapuchino.Name = "radioKapuchino";
            this.radioKapuchino.Size = new System.Drawing.Size(72, 17);
            this.radioKapuchino.TabIndex = 3;
            this.radioKapuchino.TabStop = true;
            this.radioKapuchino.Text = "Капучино";
            this.radioKapuchino.UseVisualStyleBackColor = true;
            // 
            // radioLatte
            // 
            this.radioLatte.AutoSize = true;
            this.radioLatte.Location = new System.Drawing.Point(6, 81);
            this.radioLatte.Name = "radioLatte";
            this.radioLatte.Size = new System.Drawing.Size(55, 17);
            this.radioLatte.TabIndex = 2;
            this.radioLatte.TabStop = true;
            this.radioLatte.Text = "Латте";
            this.radioLatte.UseVisualStyleBackColor = true;
            // 
            // radioEspresso
            // 
            this.radioEspresso.AutoSize = true;
            this.radioEspresso.Location = new System.Drawing.Point(6, 49);
            this.radioEspresso.Name = "radioEspresso";
            this.radioEspresso.Size = new System.Drawing.Size(74, 17);
            this.radioEspresso.TabIndex = 1;
            this.radioEspresso.TabStop = true;
            this.radioEspresso.Text = "Эспрессо";
            this.radioEspresso.UseVisualStyleBackColor = true;
            // 
            // radioWater
            // 
            this.radioWater.AutoSize = true;
            this.radioWater.Location = new System.Drawing.Point(6, 19);
            this.radioWater.Name = "radioWater";
            this.radioWater.Size = new System.Drawing.Size(50, 17);
            this.radioWater.TabIndex = 0;
            this.radioWater.Text = "Вода";
            this.radioWater.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.radioWater.UseVisualStyleBackColor = true;
            this.radioWater.CheckedChanged += new System.EventHandler(this.radioWater_CheckedChanged);
            // 
            // foodBox
            // 
            this.foodBox.Controls.Add(this.label7);
            this.foodBox.Controls.Add(this.radioBread);
            this.foodBox.Controls.Add(this.label8);
            this.foodBox.Controls.Add(this.radioBun);
            this.foodBox.Controls.Add(this.label9);
            this.foodBox.Controls.Add(this.radioChips);
            this.foodBox.Controls.Add(this.label10);
            this.foodBox.Controls.Add(this.radioCookies);
            this.foodBox.Location = new System.Drawing.Point(231, 23);
            this.foodBox.Name = "foodBox";
            this.foodBox.Size = new System.Drawing.Size(183, 207);
            this.foodBox.TabIndex = 1;
            this.foodBox.TabStop = false;
            this.foodBox.Text = "Еда";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(135, 117);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "29 руб.";
            // 
            // radioBread
            // 
            this.radioBread.AutoSize = true;
            this.radioBread.Location = new System.Drawing.Point(6, 19);
            this.radioBread.Name = "radioBread";
            this.radioBread.Size = new System.Drawing.Size(50, 17);
            this.radioBread.TabIndex = 12;
            this.radioBread.TabStop = true;
            this.radioBread.Text = "Хлеб";
            this.radioBread.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(135, 85);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "34 руб.";
            // 
            // radioBun
            // 
            this.radioBun.AutoSize = true;
            this.radioBun.Location = new System.Drawing.Point(6, 49);
            this.radioBun.Name = "radioBun";
            this.radioBun.Size = new System.Drawing.Size(66, 17);
            this.radioBun.TabIndex = 13;
            this.radioBun.TabStop = true;
            this.radioBun.Text = "Булочка";
            this.radioBun.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(135, 53);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(42, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "15 руб.";
            // 
            // radioChips
            // 
            this.radioChips.AutoSize = true;
            this.radioChips.Location = new System.Drawing.Point(6, 81);
            this.radioChips.Name = "radioChips";
            this.radioChips.Size = new System.Drawing.Size(59, 17);
            this.radioChips.TabIndex = 14;
            this.radioChips.TabStop = true;
            this.radioChips.Text = "Чипсы";
            this.radioChips.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(135, 21);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(42, 13);
            this.label10.TabIndex = 16;
            this.label10.Text = "10 руб.";
            // 
            // radioCookies
            // 
            this.radioCookies.AutoSize = true;
            this.radioCookies.Location = new System.Drawing.Point(6, 113);
            this.radioCookies.Name = "radioCookies";
            this.radioCookies.Size = new System.Drawing.Size(68, 17);
            this.radioCookies.TabIndex = 15;
            this.radioCookies.TabStop = true;
            this.radioCookies.Text = "Печенье";
            this.radioCookies.UseVisualStyleBackColor = true;
            // 
            // addWaterBox
            // 
            this.addWaterBox.Controls.Add(this.countSugar);
            this.addWaterBox.Controls.Add(this.label16);
            this.addWaterBox.Controls.Add(this.label15);
            this.addWaterBox.Controls.Add(this.label14);
            this.addWaterBox.Controls.Add(this.checkSugar);
            this.addWaterBox.Controls.Add(this.checkSyrup);
            this.addWaterBox.Controls.Add(this.checkMilk);
            this.addWaterBox.Location = new System.Drawing.Point(24, 256);
            this.addWaterBox.Name = "addWaterBox";
            this.addWaterBox.Size = new System.Drawing.Size(183, 114);
            this.addWaterBox.TabIndex = 2;
            this.addWaterBox.TabStop = false;
            this.addWaterBox.Text = "Добавки к напиткам";
            // 
            // countSugar
            // 
            this.countSugar.Location = new System.Drawing.Point(7, 81);
            this.countSugar.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.countSugar.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.countSugar.Name = "countSugar";
            this.countSugar.Size = new System.Drawing.Size(46, 20);
            this.countSugar.TabIndex = 4;
            this.countSugar.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(141, 81);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(36, 13);
            this.label16.TabIndex = 25;
            this.label16.Text = "3 руб.";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(141, 59);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(36, 13);
            this.label15.TabIndex = 24;
            this.label15.Text = "5 руб.";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(141, 35);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(42, 13);
            this.label14.TabIndex = 23;
            this.label14.Text = "10 руб.";
            // 
            // checkSugar
            // 
            this.checkSugar.AutoSize = true;
            this.checkSugar.Location = new System.Drawing.Point(58, 82);
            this.checkSugar.Name = "checkSugar";
            this.checkSugar.Size = new System.Drawing.Size(56, 17);
            this.checkSugar.TabIndex = 2;
            this.checkSugar.Text = "Сахар";
            this.checkSugar.UseVisualStyleBackColor = true;
            // 
            // checkSyrup
            // 
            this.checkSyrup.AutoSize = true;
            this.checkSyrup.Location = new System.Drawing.Point(7, 58);
            this.checkSyrup.Name = "checkSyrup";
            this.checkSyrup.Size = new System.Drawing.Size(57, 17);
            this.checkSyrup.TabIndex = 1;
            this.checkSyrup.Text = "Сироп";
            this.checkSyrup.UseVisualStyleBackColor = true;
            // 
            // checkMilk
            // 
            this.checkMilk.AutoSize = true;
            this.checkMilk.Location = new System.Drawing.Point(7, 34);
            this.checkMilk.Name = "checkMilk";
            this.checkMilk.Size = new System.Drawing.Size(65, 17);
            this.checkMilk.TabIndex = 0;
            this.checkMilk.Text = "Молоко";
            this.checkMilk.UseVisualStyleBackColor = true;
            this.checkMilk.CheckedChanged += new System.EventHandler(this.checkMilk_CheckedChanged);
            // 
            // addFoodBox
            // 
            this.addFoodBox.Controls.Add(this.label11);
            this.addFoodBox.Controls.Add(this.checkJam);
            this.addFoodBox.Controls.Add(this.label12);
            this.addFoodBox.Controls.Add(this.checkChees);
            this.addFoodBox.Controls.Add(this.label13);
            this.addFoodBox.Controls.Add(this.checkHam);
            this.addFoodBox.Location = new System.Drawing.Point(231, 256);
            this.addFoodBox.Name = "addFoodBox";
            this.addFoodBox.Size = new System.Drawing.Size(183, 114);
            this.addFoodBox.TabIndex = 3;
            this.addFoodBox.TabStop = false;
            this.addFoodBox.Text = "Добавки к еде";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(135, 82);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(36, 13);
            this.label11.TabIndex = 22;
            this.label11.Text = "7 руб.";
            // 
            // checkJam
            // 
            this.checkJam.AutoSize = true;
            this.checkJam.Location = new System.Drawing.Point(6, 80);
            this.checkJam.Name = "checkJam";
            this.checkJam.Size = new System.Drawing.Size(57, 17);
            this.checkJam.TabIndex = 5;
            this.checkJam.Text = "Джем";
            this.checkJam.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(135, 58);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(42, 13);
            this.label12.TabIndex = 21;
            this.label12.Text = "10 руб.";
            // 
            // checkChees
            // 
            this.checkChees.AutoSize = true;
            this.checkChees.Location = new System.Drawing.Point(6, 58);
            this.checkChees.Name = "checkChees";
            this.checkChees.Size = new System.Drawing.Size(47, 17);
            this.checkChees.TabIndex = 4;
            this.checkChees.Text = "Сыр";
            this.checkChees.UseVisualStyleBackColor = true;
            this.checkChees.CheckedChanged += new System.EventHandler(this.checkChees_CheckedChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(135, 35);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(42, 13);
            this.label13.TabIndex = 20;
            this.label13.Text = "15 руб.";
            // 
            // checkHam
            // 
            this.checkHam.AutoSize = true;
            this.checkHam.Location = new System.Drawing.Point(6, 34);
            this.checkHam.Name = "checkHam";
            this.checkHam.Size = new System.Drawing.Size(67, 17);
            this.checkHam.TabIndex = 3;
            this.checkHam.Text = "Ветчина";
            this.checkHam.UseVisualStyleBackColor = true;
            this.checkHam.CheckedChanged += new System.EventHandler(this.checkHam_CheckedChanged);
            // 
            // checkComplex
            // 
            this.checkComplex.Location = new System.Drawing.Point(24, 396);
            this.checkComplex.Name = "checkComplex";
            this.checkComplex.Size = new System.Drawing.Size(239, 55);
            this.checkComplex.TabIndex = 4;
            this.checkComplex.Text = "Комплекс (1 напиток с любой добавкой + 1 еда с любой добавкой на выбор)";
            this.checkComplex.UseVisualStyleBackColor = true;
            this.checkComplex.CheckedChanged += new System.EventHandler(this.checkComplex_CheckedChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(360, 416);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(42, 13);
            this.label17.TabIndex = 23;
            this.label17.Text = "90 руб.";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(21, 489);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(65, 13);
            this.label18.TabIndex = 24;
            this.label18.Text = "Стоимость:";
            // 
            // textPrice
            // 
            this.textPrice.Location = new System.Drawing.Point(101, 486);
            this.textPrice.Name = "textPrice";
            this.textPrice.Size = new System.Drawing.Size(100, 20);
            this.textPrice.TabIndex = 25;
            // 
            // issueButton
            // 
            this.issueButton.Location = new System.Drawing.Point(231, 483);
            this.issueButton.Name = "issueButton";
            this.issueButton.Size = new System.Drawing.Size(183, 23);
            this.issueButton.TabIndex = 26;
            this.issueButton.Text = "Выдать";
            this.issueButton.UseVisualStyleBackColor = true;
            this.issueButton.Click += new System.EventHandler(this.issueButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(452, 549);
            this.Controls.Add(this.issueButton);
            this.Controls.Add(this.textPrice);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.checkComplex);
            this.Controls.Add(this.addFoodBox);
            this.Controls.Add(this.addWaterBox);
            this.Controls.Add(this.foodBox);
            this.Controls.Add(this.waterBox);
            this.Name = "Form1";
            this.Text = "Form1";
            this.waterBox.ResumeLayout(false);
            this.waterBox.PerformLayout();
            this.foodBox.ResumeLayout(false);
            this.foodBox.PerformLayout();
            this.addWaterBox.ResumeLayout(false);
            this.addWaterBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.countSugar)).EndInit();
            this.addFoodBox.ResumeLayout(false);
            this.addFoodBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox waterBox;
        private System.Windows.Forms.GroupBox foodBox;
        private System.Windows.Forms.GroupBox addWaterBox;
        private System.Windows.Forms.GroupBox addFoodBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton radioGreenTea;
        private System.Windows.Forms.RadioButton radioBlackTea;
        private System.Windows.Forms.RadioButton radioKapuchino;
        private System.Windows.Forms.RadioButton radioLatte;
        private System.Windows.Forms.RadioButton radioEspresso;
        private System.Windows.Forms.RadioButton radioWater;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RadioButton radioBread;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.RadioButton radioBun;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.RadioButton radioChips;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.RadioButton radioCookies;
        private System.Windows.Forms.CheckBox checkSugar;
        private System.Windows.Forms.CheckBox checkSyrup;
        private System.Windows.Forms.CheckBox checkMilk;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox checkJam;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckBox checkChees;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox checkHam;
        private System.Windows.Forms.NumericUpDown countSugar;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.CheckBox checkComplex;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textPrice;
        private System.Windows.Forms.Button issueButton;
    }
}

